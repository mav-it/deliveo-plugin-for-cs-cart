<?php

/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

namespace Deliveo;

use Tygh\Registry;

/**
 * Class DeliveoClient
 * @package Deliveo
 */
class DeliveoClient
{
    protected $base_url = "https://api.deliveo.eu/",
        $method = "get",
        // $headers = ['Content-Type: application/json'],
        $api_key,
        $endpoint = 'test',
        $licence,
        $params;

    public function sendRequest()
    {
        $curl = curl_init();
        $opts = array();
        $url = $this->base_url . $this->getEndpoint() . $this->getTail();
        if ($this->method == 'get') {
            $opts[CURLOPT_HTTPGET] = 1;
        } else if ($this->method == 'post') {
            $opts[CURLOPT_POST] = true;
            $opts[CURLOPT_RETURNTRANSFER] = true;
            $opts[CURLOPT_POSTFIELDS] = http_build_query($this->getParams());
        }

        $opts[CURLOPT_URL] = $url;
        $opts[CURLOPT_RETURNTRANSFER] = true;
        $opts[CURLOPT_CONNECTTIMEOUT] = 30;
        $opts[CURLOPT_TIMEOUT] = 80;
        $opts[CURLOPT_RETURNTRANSFER] = true;
        $opts[CURLOPT_HTTPHEADER] = $this->headers;
        $opts[CURLOPT_SSL_VERIFYPEER] = true;

        curl_setopt_array($curl, $opts);

        $rbody = curl_exec($curl);

        if ($rbody === false) {
            $message = curl_error($curl);
            curl_close($curl);
        }
        $rcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        return json_decode($rbody, true);
    }

    public function getTail()
    {
        $licence = Registry::get('addons.deliveo.licence');
        $api_key = Registry::get('addons.deliveo.api_key');
        return "?licence=$licence&api_key=$api_key";
    }

    protected function setParams($params)
    {
        $this->params = $params;
    }

    protected function getParams()
    {
        return $this->params;
    }

    protected function setMethod($method)
    {
        $this->method = $method;
    }

    protected function getMethod()
    {
        return $this->method;
    }

    protected function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
    }

    protected function getEndpoint()
    {
        return $this->endpoint;
    }

    public static function encode($arr, $prefix = null)
    {
        if (!is_array($arr)) {
            return $arr;
        }

        $r = array();
        foreach ($arr as $k => $v) {
            if (is_null($v)) {
                continue;
            }

            if ($prefix && $k && !is_int($k)) {
                $k = $prefix . "[" . $k . "]";
            } else if ($prefix) {
                $k = $prefix . "[]";
            }

            if (is_array($v)) {
                $r[] = self::encode($v, $k, true);
            } else {
                $r[] = urlencode($k) . "=" . urlencode($v);
            }
        }

        return implode("&", $r);
    }

    public function testConnection()
    {
        $response = $this->sendRequest();

        if ($response['type'] == "error" || $response['msg'] != "client") {
            fn_set_notification('E', __('error'), __('invalid_auth'));
            return;
        }

        return true;
    }
    public function syncShippingOptions()
    {
        $this->endpoint = 'delivery';
        $response = $this->sendRequest();


        $default = db_get_row('SELECT * from ?:deliveo_shipping_options WHERE is_default=1');

        if ($response['type'] == "error") {
            fn_set_notification('E', __('error'), __('invalid_auth'));
            return;
        }
        db_query('DELETE FROM ?:deliveo_shipping_options');

        foreach ($response["data"] as $option) {
            db_query('INSERT INTO ?:deliveo_shipping_options ?e', [
                'id' => $option['value'],
                'name' => $option['description'],
            ]);
        }

        if (!empty($default)) {
            db_query('UPDATE ?:deliveo_shipping_options set default = 1 WHERE id=' . $default['id']);
        }
        return true;
    }

    public function sendPackages($data)
    {
        $this->endpoint = 'package/create';
        $this->method = 'post';

        foreach ($data as $row) {
            if (!isset($row['selected'])) {
                continue;
            }
            $exists = db_get_array("SELECT * from ?:deliveo_data WHERE order_id = ?i", $row['selected']);
            if (sizeof($exists) != 0) {
                fn_set_notification('E', __('error'), __('order') . ' ' . $row['selected'] . ': ' . __('addons.deliveo.already_sent'));

                continue;
            }
            $packaging_unit = 1;
            $order = fn_get_order_info($row['selected']);

            if (function_exists('fn_get_company_data')) {

                $vendor = fn_get_company_data($order['company_id']);
                if (
                    Registry::get('addons.deliveo.sender_address_by_vendor')  == "Y" && strlen($vendor['company']) &&
                    // strlen($vendor['country']) &&
                    strlen($vendor['zipcode']) &&
                    strlen($vendor['city']) &&
                    strlen($vendor['address'])
                ) {
                    $sender = [
                        "sender" => $vendor['company'],
                        "sender_country" => strlen($vendor['country']) ? $vendor['country'] : substr(Registry::get('addons.deliveo.sender_country_code'), 0, 2),
                        "sender_zip" => $vendor['zipcode'],
                        "sender_city" => $vendor['city'],
                        "sender_address" => $vendor['address'],
                        "sender_phone" => $vendor['phone'],
                        "sender_email" => $vendor['email'],

                    ];
                }
            } else {
                $sender = [
                    "sender" => Registry::get('addons.deliveo.sender_name'),
                    "sender_country" => substr(Registry::get('addons.deliveo.sender_country_code'), 0, 2),
                    "sender_zip" => Registry::get('addons.deliveo.sender_postcode'),
                    "sender_city" => Registry::get('addons.deliveo.sender_city'),
                    "sender_address" => Registry::get('addons.deliveo.sender_address'),
                    "sender_phone" => Registry::get('addons.deliveo.sender_phone'),
                    "sender_apartment" => Registry::get('addons.deliveo.sender_apartment'),
                    "sender_email" => Registry::get('addons.deliveo.sender_email'),

                ];
            }

            if (strlen($order['firstname'] . ' ' . $order['last_name']) < 5) {
                $name = $order['b_firstname'] . ' ' . $order['b_last_name'];
            } else {
                $name = $order['firstname'] . ' ' . $order['last_name'];
            }

            $packageData = [
                "consignee" => $name,
                "consignee_country" => $order['s_country'],
                "consignee_zip" => $order['s_zipcode'],
                "consignee_city" => $order['s_city'],
                "consignee_address" => $order['s_address'],
                "consignee_phone" => $order['s_phone'],
                "consignee_apartment" => $order['s_address_2'],
                "consignee_email" => $order['email'],
                "delivery" => (int) $row['shipping_option'],
                "priority" => Registry::get('addons.deliveo.priority'),
                "saturday" => Registry::get('addons.deliveo.saturday_deliver'),
                "referenceid" => Registry::get('addons.deliveo.reference_id_is_order_id') == 'Y' ? $row['selected'] : '',
                "tracking" => Registry::get('addons.deliveo.tracking_id_is_order_id') == 'Y' ? $row['selected'] : '',
                "insurance" => Registry::get('addons.deliveo.insurance') ? Registry::get('addons.deliveo.currency_multiplier') * $order['total'] : '',
                "freight" => 'felado',
                "packaging_unit" => sizeof($order['products']),
            ];
            $phone = $order['phone'];
            if (strlen($phone) < 3) {
                $phone = $order['s_phone'];
            }

            if (strlen($phone) < 3) {
                $phone = $order['b_phone'];
            }
            $packageData['consignee_phone'] = $phone;
        

            $packageData = array_merge($sender, $packageData);

            foreach ($order['products'] as $line_item) {
                for ($i = 1; $i <= $line_item['amount']; $i++) {
                    $packageData['packages'][] = [
                        "weight" => Registry::get('addons.deliveo.Weight'),
                        "x" => Registry::get('addons.deliveo.height') ?? 1,
                        "y" => Registry::get('addons.deliveo.width') ?? 1,
                        "z" => Registry::get('addons.deliveo.depth') ?? 1,
                        "customcode" => $line_item['product_code'],
                        "item_no" => $line_item['customcode'] . '-' . $i,
                    ];
                }
            }


            if (array_key_exists($order['payment_id'], Registry::get('addons.deliveo.admin_get_payment_methods'))) {
                $packageData["cod"];
            } else {
                $packageData["cod"] = Registry::get('addons.deliveo.currency_multiplier') * $order['total'];
            }

            $this->params = $packageData;
            $rsp = $this->sendRequest();
            if ($rsp['type'] == "success") {
                $data = [
                    'order_id' => $row['selected'],
                    'packaging_unit' => $packaging_unit,
                    'shipping_option' => $row['shipping_option'],
                    'group_id' => $rsp['data'][0],
                ];
                db_query("INSERT INTO ?:deliveo_data ?e", $data);

                if (strlen(Registry::get('addons.deliveo.change_status_to') != "")) {
                    fn_change_order_status($row["selected"], strtoupper(Registry::get('addons.deliveo.change_status_to')));
                }
                fn_set_notification('N', __('notice'),  _('order') . ' ' . $row['selected'] . ' ' . __('addons.deliveo.successfully_sent') . ': ' . $rsp['data'][0]);
            } else {
                fn_set_notification('E', __('error'), _('order') . ' ' . $row['selected'] . ': ' . $rsp['msg']);
            }
        }

        return true;
    }


    public function getLog($group_id)
    {
        $this->method = "get";
        $this->endpoint = "package_log/$group_id";
        $rsp = $this->sendRequest();
        return $rsp;
    }

    public function checkSignature($group_id)
    {
        $this->method = "get";
        $this->endpoint = "package/$group_id";

        $rsp = $this->sendRequest();
        $signature = null;

        if (isset($rsp['data']) && isset($rsp['data'][0])) {
            $signed = !is_null($rsp['data'][0]['dropped_off']) ? true : false;
        }
        $this->endpoint = "signature/$group_id";

        if ($signed) {
            $signature = $this->base_url . $this->getEndpoint() . $this->getTail();
        }

        return $signature;
    }
}