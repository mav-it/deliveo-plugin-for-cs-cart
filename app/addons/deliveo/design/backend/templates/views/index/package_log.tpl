<h2>{$group_id}</h2>
<table class="table table-middle w-100 table-manage-logs">
    <thead>
        <tr>
            <th>{__('time')}</th>
            <th>{__('status')}</th>
        </tr>
    </thead>
    <tbody>
        {foreach from=$data['data'] item=row}
        <tr>
            <td>{date('Y-m-d H:i',$row.timestamp)}</td>
            <td>{$row.status_text}</td>
        </tr>
        {/foreach}
    </tbody>
    {if (!is_null($signature))}
    <tfoot>
        <tr>
            <td colspan="2" style="text-align:right">
                <a class="btn btn-primary" target="_blank" href="{$signature}">{{__("signature_sheet")}}</a>
            </td>
        </tr>
    </tfoot>
    {/if}
</table>