<?php

/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

$schema['top']['administration']['items']['deliveo'] = [
    'attrs' => [
        'class' => 'is-addon',
    ],
    'href' => 'deliveo.orders?pending=N',
    'title' => 'Deliveo BETA ' . '- ' . __('addons.deliveo.sent_packages'),
    'position' => 406,
];

$schema['top']['administration']['items']['deliveo_pending'] = [
    'attrs' => [
        'class' => 'is-addon',
    ],
    'href' => 'deliveo.orders?pending=Y',
    'title' => 'Deliveo BETA ' . '- '  . __('addons.deliveo.pending_shipments'),
    'position' => 407,
];

return $schema;
