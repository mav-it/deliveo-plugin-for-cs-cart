<?php

use Deliveo\DeliveoClient;

function fn_settings_variants_addons_deliveo_admin_send_order_statuses()
{
    return fn_deliveo_get_order_statuses();
}




function fn_settings_variants_addons_deliveo_admin_get_options()
{
    $deliveo = new DeliveoClient;
    $deliveo->syncShippingOptions();
    return fn_deliveo_get_deliveo_shipping_options();
}
