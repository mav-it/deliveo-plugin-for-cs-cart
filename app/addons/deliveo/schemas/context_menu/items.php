<?php

/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use Tygh\ContextMenu\Items\ComponentItem;
use Tygh\ContextMenu\Items\GroupItem;

defined('BOOTSTRAP') or die('Access denied!');

$statuses = fn_get_simple_statuses(STATUSES_SHIPMENT);
// $list_status = [];
// foreach ($statuses as $key => $status) {
//     $list_status[$key] = [
//         'name' =>  [
//             'template' => $status,
//         ],
//         'dispatch' =>  'deliveo.orders?status=' . $key,
//         'data'     => [
//             'action_attributes' => [
//                 // 'class'               => 'cm-ajax cm-post cm-ajax-send-form',
//                 'href'                => fn_url('deliveo.orders?status=' . $key),
//                 'data-ca-target-id'   => 'pagination_contents',
//                 'data-ca-target-form' => '#banners_form',
//             ],
//         ],
//         'params' => [
//             'status' => $key
//         ]
//     ];
// }
$selectable_statuses = fn_get_default_statuses('', false);

$schema = [
    'selectable_statuses' => $selectable_statuses,
    'items' => [
        'status' => [
            'name' => ['template' => 'status'],
            'type' => GroupItem::class,
            'items' => [],
            'position' => 20,
        ],
        'actions' => [
            'name' => ['template' => 'actions'],
            'type' => GroupItem::class,
            'items' => [
                'send_to_deliveo' => [
                    'name' => ['template' => 'addons.deliveo.send_to_deliveo'],
                    'dispatch' => 'deliveo.send',
                    'position' => 10,
                ],
            ],
            'position' => 40,
        ],
        // 'statuses' => [
        //     'name' => ['template' => 'addons.deliveo.list_by_status'],
        //     'type' => GroupItem::class,
        //     'items' => $list_status,
        //     'position' => 40,
        // ],
        'shipping_options' => [
            'name' => ['template' => 'addons.deliveo.default_shipping_option'],
            'type' => ComponentItem::class,
            'template' => 'addons/deliveo/views/deliveo/components/context_menu/shipping_options.tpl',
            'position' => 50,
        ],
    ],
];

if ($_REQUEST['pending'] == 'N') {
    unset($schema['items']['actions']);
    unset($schema['items']['shipping_options']);
    unset($schema['items']['statuses']);
}

return $schema;
