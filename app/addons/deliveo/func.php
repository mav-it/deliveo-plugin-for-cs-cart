<?php

/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use Deliveo\DeliveoClient;
use Tygh\Enum\YesNo;
use Tygh\Navigation\LastView;
use Tygh\Registry;

if (!defined('AREA')) {
    die('Access denied');
}

function fn_deliveo_change_order_status_post($order_id, $status_to, $status_from, $force_notification, $place_order, $order, $edp_data)
{

    if (strlen(Registry::get('addons.deliveo.admin_get_options')) > 0 && strlen(Registry::get('addons.deliveo.admin_send_order_statuses')) > 0) {

        if (Registry::get('addons.deliveo.admin_send_order_statuses') == $status_to) {

            $client = new DeliveoClient;
            $client->sendPackages([
                [
                    "selected" => $order_id,
                    "shipping_option" => Registry::get('addons.deliveo.admin_get_options')
                ]
            ]);
        }
    }
}

function fn_deliveo_get_order_statuses()
{
    return array_merge(['' => __('addons.deliveo.do_not_send')], db_get_hash_single_array("SELECT a.status, b.description FROM ?:statuses AS a LEFT JOIN ?:status_descriptions AS b ON a.status_id = b.status_id AND b.lang_code = ?s WHERE a.type = ?s", array('status', 'description'), CART_LANGUAGE, 'O'));
}

function fn_settings_variants_addons_deliveo_admin_get_payment_methods()
{
    $data =  db_get_hash_single_array("SELECT payment_id, payment FROM ?:payment_descriptions", array('payment_id', 'payment'));
    return $data;
}


function fn_deliveo_get_deliveo_shipping_options()
{
    $data = db_get_hash_single_array("SELECT id, name FROM ?:deliveo_shipping_options", array('id', 'name'), CART_LANGUAGE, 'O');

    if (sizeof($data) == 0) {
        return [
            '' => __('addons.deliveo.add_and_save_your_credentials_first')
        ];
    }

    return $data;
}


function fn_get_deliveo_statuses()
{
    $statuses = db_query("SELECT * FROM ?:deliveo_shipping_options");

    $items = [];
    foreach ($statuses as $status) {
        $items[$status['id']] = [
            'name' => ['template' => $status['name'], 'params' => [
                'id' => $status['id'],
                'is_default' => $status['is_default'],
            ]],
            'dispatch' => 'deliveo.shipping_option',
            'position' => $status['id'],
        ];
    }
    return $items;
}

function fn_get_deliveo_label($group_id)
{
    $licence = Registry::get('addons.deliveo.licence');
    $api_key = Registry::get('addons.deliveo.api_key');

    $url = "https://api.deliveo.eu/label/" . $group_id . "?licence=$licence&api_key=$api_key";
    return $url;
}

/**
 * Returns deliveo orders
 *
 * @param array  $params         Orders search params
 * @param int    $items_per_page Items per page
 *
 * @return array
 */
function fn_get_deliveo_orders(array $params, $items_per_page = 0, $onlySent = false)
{

    // Init filter
    $params = LastView::instance()->update('orders', $params);

    if (isset($params['extra'])) {
        $params['extra'] = (array) $params['extra'];
    }

    // Set default values to input params
    $default_params = [
        'page' => 1,
        'items_per_page' => $items_per_page,
        'extra' => ['issuers', 'invoice_docs', 'memo_docs'],
    ];

    $params = array_merge($default_params, $params);

    if (!empty($params['issuer']) && !in_array('issuers', $params['extra'], true)) {
        $params['extra'][] = 'issuers';
    }

    if ((!empty($params['invoice_id']) || !empty($params['has_invoice']))
        && !in_array('invoice_docs', $params['extra'], true)
    ) {
        $params['extra'][] = 'invoice_docs';
    }

    if ((!empty($params['credit_memo_id']) || !empty($params['has_credit_memo']))
        && !in_array('memo_docs', $params['extra'], true)
    ) {
        $params['extra'][] = 'memo_docs';
    }

    if (AREA != 'C') {
        $params['include_incompleted'] = empty($params['include_incompleted']) ? false : $params['include_incompleted']; // default incomplited orders should not be displayed
        if (!empty($params['status']) && (is_array($params['status']) && in_array(STATUS_INCOMPLETED_ORDER, $params['status']) || !is_array($params['status']) && $params['status'] == STATUS_INCOMPLETED_ORDER)) {
            $params['include_incompleted'] = true;
        }
    } else {
        $params['include_incompleted'] = false;
    }

    // Define fields that should be retrieved
    $fields = [
        '?:orders.order_id',
        '?:orders.issuer_id',
        '?:orders.user_id',
        '?:orders.is_parent_order',
        '?:orders.parent_order_id',
        '?:orders.company_id',
        '?:orders.company',
        '?:orders.timestamp',
        '?:orders.firstname',
        '?:orders.lastname',
        '?:orders.email',
        '?:orders.company',
        '?:orders.phone',
        '?:orders.status',
        '?:orders.total',
        '?:deliveo_data.packaging_unit',
        '?:deliveo_data.shipping_option',
        '?:deliveo_data.group_id',
    ];

    // Define sort fields
    $sortings = [
        'order_id' => '?:orders.order_id',
        'status' => '?:orders.status',
        'customer' => ['?:orders.lastname', '?:orders.firstname'],
        'email' => '?:orders.email',
        'date' => ['?:orders.timestamp', '?:orders.order_id'],
        'total' => '?:orders.total',
    ];

    if (in_array('issuers', $params['extra'], true)) {
        $fields[] = "CONCAT(issuers.firstname, ' ', issuers.lastname) as issuer_name";
        $fields[] = 'issuers.email as issuer_email';
    }

    if (in_array('invoice_docs', $params['extra'], true)) {
        $fields[] = 'invoice_docs.doc_id as invoice_id';
    }

    if (in_array('memo_docs', $params['extra'], true)) {
        $fields[] = 'memo_docs.doc_id as credit_memo_id';
    }

    if (isset($params['compact']) && $params['compact'] === YesNo::YES) {
        $union_condition = ' OR ';
    } else {
        $union_condition = ' AND ';
    }

    $condition = $_condition = $join = $group = '';


    if ($onlySent == true) {
        $condition .= db_quote(' AND ?:deliveo_data.group_id is NOT NULL');
    } else {
        $condition .= db_quote(' AND ?:deliveo_data.group_id is NULL');
    }
    // $condition .= db_quote(' AND ?:orders.is_parent_order != ?s', YesNo::YES);
    $condition .= fn_get_company_condition('?:orders.company_id');

    if (in_array('issuers', $params['extra'], true)) {
        $join = db_quote(' LEFT JOIN ?:users as issuers ON issuers.user_id = ?:orders.issuer_id');
    }

    if (in_array('invoice_docs', $params['extra'], true)) {
        $join .= " LEFT JOIN ?:order_docs as invoice_docs ON invoice_docs.order_id = ?:orders.order_id AND invoice_docs.type = 'I'";
    }

    if (in_array('memo_docs', $params['extra'], true)) {
        $join .= " LEFT JOIN ?:order_docs as memo_docs ON memo_docs.order_id = ?:orders.order_id AND memo_docs.type = 'C'";
    }

    $join .= ' LEFT join ?:deliveo_data ON (?:deliveo_data.order_id = ?:orders.order_id)';

    if (isset($params['phone']) && !empty($params['phone'])) {
        $phone = '%' . $params['phone'] . '%';
        $condition .= db_quote(
            " AND ((REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(?:orders.phone, ' ', ''), '-', ''), '+', ''), '(', ''), ')', '') LIKE ?l)"
                . " OR (REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(b_phone, ' ', ''), '-', ''), '+', ''), '(', ''), ')', '') LIKE ?l)"
                . " OR (REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(s_phone, ' ', ''), '-', ''), '+', ''), '(', ''), ')', '') LIKE ?l))",
            $phone,
            $phone,
            $phone
        );
    }

    if (isset($params['cname']) && fn_string_not_empty($params['cname'])) {
        $customer_name = fn_explode(' ', $params['cname']);
        $customer_name = array_filter($customer_name, 'fn_string_not_empty');
        if (sizeof($customer_name) == 2) {
            $_condition .= db_quote(
                ' ?p ((?:orders.firstname LIKE ?l AND ?:orders.lastname LIKE ?l)'
                    . ' OR (?:orders.firstname LIKE ?l AND ?:orders.lastname LIKE ?l))',
                $union_condition,
                '%' . $customer_name[0] . '%',
                '%' . $customer_name[1] . '%',
                '%' . $customer_name[1] . '%',
                '%' . $customer_name[0] . '%'
            );
        } else {
            $_condition .= db_quote(
                ' ?p (?:orders.firstname LIKE ?l OR ?:orders.lastname LIKE ?l)',
                $union_condition,
                '%' . trim($params['cname']) . '%',
                '%' . trim($params['cname']) . '%'
            );
        }
    }

    if (isset($params['issuer']) && fn_string_not_empty($params['issuer'])) {
        $issuer_name = fn_explode(' ', $params['issuer']);
        $issuer_name = array_filter($issuer_name, "fn_string_not_empty");
        if (sizeof($issuer_name) == 2) {
            $_condition .= db_quote(" $union_condition (issuers.firstname LIKE ?l AND issuers.lastname LIKE ?l)", array_shift($issuer_name) . "%", "%" . array_shift($issuer_name) . "%");
        } else {
            $issuer_name = array_shift($issuer_name);
            $_condition .= db_quote(" $union_condition (issuers.firstname LIKE ?l OR issuers.lastname LIKE ?l)", $issuer_name . "%", "%" . $issuer_name . "%");
        }
    }

    if (!empty($params['no_issuer'])) {
        $condition .= db_quote(' AND ?:orders.issuer_id IS NULL');
    }

    if (isset($params['company_id']) && $params['company_id'] != '') {
        $condition .= db_quote(' AND ?:orders.company_id = ?i ', $params['company_id']);
    }

    if (!empty($params['company_ids']) && is_array($params['company_ids'])) {
        $condition .= db_quote(' AND ?:orders.company_id IN (?n)', $params['company_ids']);
    }

    if (!empty($params['tax_exempt'])) {
        $condition .= db_quote(" AND ?:orders.tax_exempt = ?s", $params['tax_exempt']);
    }

    if (!empty($params['company'])) {
        $condition .= db_quote(" AND ?:orders.company LIKE ?l", '%' . $params['company'] . '%');
    }

    if (isset($params['email']) && fn_string_not_empty($params['email'])) {
        $_condition .= db_quote(" $union_condition ?:orders.email LIKE ?l", "%" . trim($params['email']) . "%");
    }

    if (!empty($params['user_id'])) {
        $condition .= db_quote(' AND ?:orders.user_id IN (?n)', $params['user_id']);
    }

    if (isset($params['total_from']) && fn_is_numeric($params['total_from'])) {
        $condition .= db_quote(" AND ?:orders.total >= ?d", fn_convert_price($params['total_from']));
    }

    if (!empty($params['total_to']) && fn_is_numeric($params['total_to'])) {
        $condition .= db_quote(" AND ?:orders.total <= ?d", fn_convert_price($params['total_to']));
    }

    if (isset($params['total_sec_from']) && fn_is_numeric($params['total_sec_from'])) {
        $condition .= db_quote(" AND ?:orders.total >= ?d", fn_convert_price($params['total_sec_from'], CART_SECONDARY_CURRENCY));
    }

    if (!empty($params['total_sec_to']) && fn_is_numeric($params['total_sec_to'])) {
        $condition .= db_quote(" AND ?:orders.total <= ?d", fn_convert_price($params['total_sec_to'], CART_SECONDARY_CURRENCY));
    }

    if (!empty($params['status'])) {
        // $condition .= db_quote(' AND ?:orders.status IN (?a)', $params['status']);
    }

    if (empty($params['include_incompleted'])) {
        // $condition .= db_quote(' AND ?:orders.status != ?s', STATUS_INCOMPLETED_ORDER);
    }

    if (!empty($params['storefront_id'])) {
        $condition .= db_quote(' AND ?:orders.storefront_id IN (?n)', (array) $params['storefront_id']);
    }

    if (!empty($params['order_id'])) {
        $_condition .= db_quote($union_condition . ' ?:orders.order_id IN (?n)', (!is_array($params['order_id']) && (strpos($params['order_id'], ',') !== false) ? explode(',', $params['order_id']) : $params['order_id']));
    }

    if (!empty($params['p_ids']) || !empty($params['product_view_id'])) {
        $arr = (strpos($params['p_ids'], ',') !== false || !is_array($params['p_ids'])) ? explode(',', $params['p_ids']) : $params['p_ids'];

        if (empty($params['product_view_id'])) {
            $condition .= db_quote(" AND ?:order_details.product_id IN (?n)", $arr);
        } else {
            $condition .= db_quote(" AND ?:order_details.product_id IN (?n)", db_get_fields(fn_get_products(array('view_id' => $params['product_view_id'], 'get_query' => true))));
        }

        $join .= " LEFT JOIN ?:order_details ON ?:order_details.order_id = ?:orders.order_id";
        $group = " GROUP BY ?:orders.order_id ";
    }

    $docs_conditions = array();
    if (!empty($params['invoice_id']) || !empty($params['has_invoice'])) {
        if (!empty($params['has_invoice'])) {
            $docs_conditions[] = "invoice_docs.doc_id IS NOT NULL";
        } elseif (!empty($params['invoice_id'])) {
            $docs_conditions[] = db_quote("invoice_docs.doc_id = ?i", $params['invoice_id']);
        }
    }

    if (!empty($params['credit_memo_id']) || !empty($params['has_credit_memo'])) {
        if (!empty($params['has_credit_memo'])) {
            $docs_conditions[] = "memo_docs.doc_id IS NOT NULL";
        } elseif (!empty($params['credit_memo_id'])) {
            $docs_conditions[] = db_quote("memo_docs.doc_id = ?i", $params['credit_memo_id']);
        }
    }

    if (!empty($docs_conditions)) {
        $condition .= ' AND (' . implode(' OR ', $docs_conditions) . ')';
    }

    if (!empty($params['shippings'])) {
        $set_conditions = array();
        foreach ($params['shippings'] as $v) {
            $set_conditions[] = db_quote("FIND_IN_SET(?s, ?:orders.shipping_ids)", $v);
        }
        $condition .= ' AND (' . implode(' OR ', $set_conditions) . ')';
    }

    if (!empty($params['payments'])) {
        $condition .= db_quote(" AND ?:orders.payment_id IN (?n)", $params['payments']);
    }

    if (!empty($params['period']) && $params['period'] != 'A') {
        list($params['time_from'], $params['time_to']) = fn_create_periods($params);

        $condition .= db_quote(" AND (?:orders.timestamp >= ?i AND ?:orders.timestamp <= ?i)", $params['time_from'], $params['time_to']);
    }

    if (isset($params['updated_at_from'])) {
        $condition .= db_quote(' AND ?:orders.updated_at >= ?i', $params['updated_at_from']);
    }

    if (isset($params['updated_at_to'])) {
        $condition .= db_quote(' AND ?:orders.updated_at <= ?i', $params['updated_at_to']);
    }

    if (!empty($params['custom_files']) && $params['custom_files'] == 'Y') {
        $condition .= db_quote(" AND ?:order_details.extra LIKE ?l", '%custom_files%');

        if (empty($params['p_ids']) && empty($params['product_view_id'])) {
            $join .= " LEFT JOIN ?:order_details ON ?:order_details.order_id = ?:orders.order_id";
            $group = " GROUP BY ?:orders.order_id ";
        }
    }

    if (!empty($params['parent_order_id'])) {
        $condition .= db_quote(' AND ?:orders.parent_order_id IN (?n)', (array) $params['parent_order_id']);
    }

    if (!empty($params['company_name'])) {
        $fields[] = '?:companies.company as company_name';
        $join .= " LEFT JOIN ?:companies ON ?:companies.company_id = ?:orders.company_id";
    }

    if (!empty($_condition)) {
        $condition .= ' AND (' . ($union_condition == ' OR ' ? '0 ' : '1 ') . $_condition . ')';
    }

    fn_set_hook('get_orders', $params, $fields, $sortings, $condition, $join, $group);

    $sorting = db_sort($params, $sortings, 'date', 'desc');

    // Used for Extended search
    if (!empty($params['get_conditions'])) {
        return array($fields, $join, $condition);
    }

    $limit = '';
    if (!empty($params['items_per_page'])) {
        $limit = db_paginate($params['page'], $params['items_per_page']);
        $params['total_items'] = db_get_field(
            'SELECT COUNT(DISTINCT (?:orders.order_id))'
                . ' FROM ?:orders'
                . ' ?p'
                . ' WHERE 1 ?p',
            $join,
            $condition
        );
    }

    $orders = db_get_array('SELECT ' . implode(', ', $fields) . " FROM ?:orders $join WHERE 1 $condition $group $sorting $limit");

    fn_set_hook('get_orders_post', $params, $orders);

    foreach ($orders as $k => $order) {
        if (isset($order['ip_address'])) {
            $order['ip_address'] = fn_ip_from_db($order['ip_address']);
        }
    }


    $totals = [];

    LastView::instance()->processResults('orders', $orders, $params);

    return [$orders, $params, $totals];
}
