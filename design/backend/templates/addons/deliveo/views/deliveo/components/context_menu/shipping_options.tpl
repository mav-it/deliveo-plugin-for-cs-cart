{capture assign="content"}
<div class="bulk-edit-inner__header">
    {if ($shipping_options)}
    <span>{__($data.name.template, $data.name.params)}</span>
    {else}
    <span>{__('invalid_auth')}</span>
    {/if}
</div>
{if ($shipping_options)}
<form method="post" action="{'change_default_shipping_option'|fn_url}" name="change_default_shipping_option">
<div class="bulk-edit-inner__body">
    <div class="bulk-edit-inner__input-group">
        <select name="default_shipping_option" class="input-group__modifier" style="min-width:250px;width: 100%;" data-ca-bulkedit-mod-option-filter-a>
            {foreach from=$shipping_options item="option"}
            <option {if ($option['name']['params']['is_default']==1)}selected {/if} value="{$option['name']['params']['id']}">{$option['name']['template']}</option>
            {/foreach}
        </select>
                    
    </div>

                                        <label>
<input type="checkbox" name="set_to_default" value="1" /> 
                                        {__('saved_search.set_as_default')}</label>
</div>
</form>

<div class="bulk-edit-inner__footer">
    <button class="btn btn-primary bulk-edit-inner__btn bulkedit-mod-update" role="button" data-ca-bulkedit-mod-update data-ca-bulkedit-mod-values="[data-ca-bulkedit-mod-changer]" data-ca-bulkedit-mod-target-form="[name=change_default_shipping_option]" data-ca-bulkedit-mod-dispatch="deliveo.change_default_shipping_option">{__("apply")}</button>
</div>
{/if}
{/capture}
<script>
$('select[name=default_shipping_option]').change(function(){
    $('table tr.selected option').removeAttr('selected')
    $('table tr.selected option[value='+$(this).val()+']').attr('selected', 'selected')
})
</script>

{include file="components/context_menu/items/dropdown.tpl"
content=$content
data=$data
id=$item_id
}