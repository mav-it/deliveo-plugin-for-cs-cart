{capture name="mainbox"}

<form action="{'deliveo.send'|fn_url}" method="post" target="_self" name="orders_list_form" id="orders_list_form" data-ca-is-multiple-submit-allowed="true">

    {include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

    {$c_url=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
    {$c_icon="<i class=\"icon-`$search.sort_order_rev`\"></i>"}
    {$c_dummy="<i class=\"icon-dummy\"></i>"}
    {$rev=$smarty.request.content_id|default:"pagination_contents"}
    {if $items}
    {capture name="deliveo_table"}
    <div class="table-responsive-wrapper longtap-selection">
        <table width="100%" class="table table-middle table--relative table-responsive table-manage-orders">
            <thead data-ca-bulkedit-default-object="true" data-ca-bulkedit-component="defaultObject">
                <tr>
                    <th width="6%" class="left mobile-hide">
                        {include file="common/check_items.tpl" check_statuses=$order_status_descr}
                        <input type="checkbox" class="bulkedit-toggler hide" data-ca-bulkedit-disable="[data-ca-bulkedit-default-object=true]" data-ca-bulkedit-enable="[data-ca-bulkedit-expanded-object=true]" />
                    </th>
                    <th width="15%"><a class="cm-ajax" href="{" `$c_url`&sort_by=order_id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("id")}{if $search.sort_by == "order_id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
                    <th width="15%">{__("addons.deliveo.group_id")}</th>
                    <th style="width:5%">{__("addons.deliveo.packaging_unit")}</th>
                    <th width="15%">{__("date")}</th>
                    <th width="17%">{__("customer")}{if $search.sort_by == "customer"}{$c_icon nofilter}{/if}</th>

                    <th width="10%" class="right"><a class="cm-ajax{if $search.sort_by == " total"} sort-link-{$search.sort_order_rev}{/if}" href="{" `$c_url`&sort_by=total&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("total")}</a></th>
                    {if ($_REQUEST['pending'] == 'N')} 
                    <th>{__('tag')}</th>
                    {/if}
                </tr>
            </thead>
            {foreach from=$items item="o"}
            <tr class="cm-longtap-target" data-ca-longtap-action="setCheckBox" data-ca-longtap-target="input.cm-item" data-ca-id="{$o.order_id}">
                <td width="6%" class="left mobile-hide">
                    <input type="checkbox" name="items[{$o.order_id}][selected]" value="{$o.order_id}" class="cm-item cm-item-status-{$o.status|lower} hide" />
                </td>
                <td width="15%" data-th='{__(" id")}'>
                    <a target="_blank" href='{"orders.details?order_id=`$o.order_id`"|fn_url}' class="underlined">{__("order")} <bdi>#{$o.order_id}</bdi></a>
                    {if $order_statuses[$o.status].params.appearance_type == "I" && $o.invoice_id}
                    <p class="muted">{__("invoice")} #{$o.invoice_id}</p>
                    {elseif $order_statuses[$o.status].params.appearance_type == "C" && $o.credit_memo_id}
                    <p class="muted">{__("credit_memo")} #{$o.credit_memo_id}</p>
                    {/if}
                    {include file="views/companies/components/company_name.tpl" object=$o}
                </td>
                <td width="15%" data-th='{__("group_id")}'>
                    {if (is_null($o.group_id))}
                    <select name="items[{$o.order_id}][shipping_option]" class="input-group__modifier shipping_option" style="width: 100%;" data-ca-bulkedit-mod-option-filter-a>
                        {foreach from=$shipping_options item="option"}
                        <option {if ($option['name']['params']['is_default']==1)}selected {/if} value="{$option['name']['params']['id']}">{$option['name']['template']}</option>
                        {/foreach}
                    </select>
                    {else}
                    <a class="cm-dialog-opener cm-ajax btn btn-primary strong" data-ca-target-id="package_log-{$o.group_id}" href='{"deliveo.package_log?group_id=`$o.group_id`"|fn_url}' target="_blank">{$o.group_id}</a>
                    {/if}
                </td>
                <td style="width:5%" data-th='{__("packaging_unit")}' >
                    {if (is_null($o.group_id))}
                    <input style="max-width: 80px;" type="number" min="1" max="{$o.max_packaging_unit}" name="items[{$o.order_id}][packaging_unit]" value="{if ($packaging_unit_type != 'one')}{$o.packaging_unit}{else}1{/if}" {if ($packaging_unit_type != 'manual')} readonly {/if}>
                    {else}
                    {$o.packaging_unit}
                    {/if}
                </td>
                <td width="15%" class="nowrap" data-th='{__("date")}'>{$o.timestamp|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}</td>
                <td width="17%" data-th='{__("customer")}'>
                    {$o.firstname} {$o.lastname}
                </td>
                <td width="10%" class="right" data-th='{__(" total")}'>
                    {include file="common/price.tpl" value=$o.total}
                </td>

                    {if ($_REQUEST['pending'] == 'N')} 
                <td>
                    {if (!is_null($o.group_id))}
                    <a href="{$o.label_url}" target="_blank">{__('Download')}</a>
                    {/if}
                </td>
                {/if}
            </tr>
            {/foreach}
        </table>
    </div>
    {/capture}

    {include file="common/context_menu_wrapper.tpl"
    form="orders_list_form"
    object="items"
    items=$smarty.capture.deliveo_table
    }

    {else}
    <p class="no-items">{__("no_data")}</p>
    {/if}

    {include file="common/pagination.tpl" div_id=$smarty.request.content_id}


</form>

{/capture}

{capture name="tools_list"}

<li class="bulkedit-action--legacy hide mobile-hide">{btn type="list" text=__("export_selected") dispatch="dispatch[orders.export_range]" form="orders_list_form"}</li>
<li class="bulkedit-action--legacy hide mobile-hide">{btn type="delete_selected" dispatch="dispatch[orders.m_delete]" form="orders_list_form"}</li>
{dropdown content=$actions class="bulkedit-dropdown--legacy hide"}
{/capture}

{include file="common/mainbox.tpl"
title=$page_title
content=$smarty.capture.mainbox
content_id="manage_orders"
}